use warp::{Filter, Rejection};

type Result<T> = std::result::Result<T, error::Error>;
type WebResult<T> = std::result::Result<T, Rejection>;

mod db;
mod error;
mod handlers;
mod models;
mod routes;

#[tokio::main]
async fn main() {
    println!("Connecting to mongodb...");
    let db = db::init_db("mongodb://root:example@127.0.0.1:27017")
        .await
        .unwrap();
    println!("Connected to mongodb.");

    let servers_routes = routes::server_routes(db);

    let routes = servers_routes.recover(error::handle_rejection);

    warp::serve(routes).run(([127, 0, 0, 1], 3030)).await;
}
