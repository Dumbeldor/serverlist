use warp::{self, http::StatusCode};

use crate::{db::Db, models::Server, WebResult};

pub async fn list_servers(db: Db) -> WebResult<impl warp::Reply> {
    let servers = db.lock().await.fetch_servers().await?;
    Ok(warp::reply::json(&servers))
}

pub async fn create_server(new_server: Server, db: Db) -> WebResult<impl warp::Reply> {
    db.lock().await.create_server(new_server).await?;
    Ok(StatusCode::CREATED)
}
