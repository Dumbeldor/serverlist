use futures::stream::StreamExt;
use mongodb::{
    bson::{from_document, to_document},
    options::ClientOptions,
    Client, Collection,
};
use std::sync::Arc;
use tokio::sync::Mutex;

use crate::{error::Error::*, models::Server, Result};

const DB_NAME: &str = "veloren";

pub type Db = Arc<Mutex<Database>>;

pub async fn init_db(url: &str) -> Result<Db> {
    let database = Database::new(url).await?;
    Ok(Arc::new(Mutex::new(database)))
}

#[derive(Clone, Debug)]
pub struct Database {
    pub client: Client,
}

impl Database {
    pub async fn new(url: &str) -> Result<Self> {
        let mut client_options = ClientOptions::parse(url).await?;
        client_options.app_name = Some("Serverlist".to_string());

        let client = Client::with_options(client_options)?;

        Ok(Self { client })
    }

    pub async fn fetch_servers(&self) -> Result<Vec<Server>> {
        let mut cursor = self
            .get_collection()
            .find(None, None)
            .await
            .map_err(MongoQueryError)?;

        let mut result: Vec<Server> = Vec::new();
        while let Some(doc) = cursor.next().await {
            result.push(from_document(doc?).map_err(DecodeBSONError)?);
        }

        Ok(result)
    }

    pub async fn create_server(&self, new_server: Server) -> Result<()> {
        let doc = to_document(&new_server).map_err(EncodeBSONError)?;
        self.get_collection()
            .insert_one(doc, None)
            .await
            .map_err(MongoQueryError)?;

        Ok(())
    }

    fn get_collection(&self) -> Collection {
        self.client.database(DB_NAME).collection("servers")
    }
}
